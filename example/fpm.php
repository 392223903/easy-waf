<?php

use EasyWaf\Enums;
use EasyWaf\Utils;
use EasyWaf\Waf;

require '../vendor/autoload.php';

// 连接redis
$redis = new Redis();
$redis->pconnect('127.0.0.1');

// 启动waf防护
$wafConfig = [
    'debug' => true,  // 是否开启debug模式,开启后将输出waf拦截信息
    'redis' => $redis,  // redis实例对象,用于CC防火墙和爬虫拦截
    'checkCc' => [
        'enable' => true,  // 是否开启CC攻击防护
        'enableSeoIpCheck' => true,  // 开启SEO-IP检查,避免误伤百度蜘蛛等优化IP,此功能需要配合cli.php示例配置
        'enableBehaviorCheck' => true,  // 开启行为验证,支持COOKIE验证
        'seoFriendly' => true,  // 是否SEO友好,不需要SEO的网站请关闭,防御效果更好
        'reqTime' => 60,  // 请求时间间隔,单位秒
        'reqCount' => 30,  // 单位时间内请求次数限制
        'reqDayMaxCount' => 300,  // 每日请求最大次数限制,0则不限制(单个IP限制)
    ],
    'illegalReqCallback' => function ($businessCode) use ($redis) {
        // 拦截后的回调函数,根据业务码进行业务处理,可不配置
        // businessCode说明
        // 100 触发请求频繁  101 触发每日请求最大次数限 102 GET参数包含非法字符串 103 POST参数包含非法字符串 104 COOKIE参数包含非法字符串
        // 105 UserAgent为空  106 UserAgent包含非法字符串  107 禁止代理IP访问  108 请求方法不合法,例如禁止POST请求 109 IP已封禁   110 IP行为异常
        // 例子, 将触发的IP加入IP黑名单,拉黑7天
        $checkCode = [100, 101, 105, 106, 107];
        if (in_array($businessCode, $checkCode)) {
            $ip = Utils::getClientRealIp();
            if ($ip) {
                $ipBlackKey = Utils::getIpCacheKey($ip, Enums::WafIpBlackListKey);
                $redis->setex($ipBlackKey, 86400 * 30, true);
            }
        }
    },
    'checkGet' => true,  // 是否开启GET参数防护,如SQL注入
    'checkPost' => true,  // 是否开启POST参数防护,如SQL注入
    'checkCookie' => true,  // 是否开启COOKIE参数防护,如SQL注入
    'checkUserAgent' => true,  // 是否开启UserAgent参数防护,如各种垃圾爬虫,扫描器等
    'checkProxyRequest' => true,  // 是否开启代理请求检查,拦截代理请求
    'checkRequestMethod' => ['get', 'post', 'option'],  // 是否开启请求方法检测,仅允许get,post,option请求方法
];
(new Waf($wafConfig))->run();
echo 'hello world';