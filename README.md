<p><h4>EasyWaf简单易用的PHP_WEB应用防火墙</h4></p>
## <h4 style="text-align:left">  项目介绍 </h4>

<p>EasyWaf是一个基于PHP的Composer包，旨在帮助开发人员轻松地加强其Web应用程序的安全性。该包提供了针对SQL注入、CC攻击防护、SEO爬虫IP真伪侦测、跨站脚本攻击（XSS）和跨站请求伪造（CSRF）的强大防护功能，让开发人员能够更好地保护他们的应用免受恶意攻击。</p>

## <h4>  Composer安装 </h4>

~~~
    composer require easy-waf/easy-waf
~~~

## <h5>【A】. 配置防护规则 </h5>
~~~
// 连接redis
$redis = new Redis();
$redis->pconnect('127.0.0.1');

// 启动waf防护
$wafConfig = [
    'debug' => true,  // 是否开启debug模式,开启后将输出waf拦截信息
    'redis' => $redis,  // redis实例对象,用于CC防火墙和爬虫拦截
    'checkCc' => [
        'enable' => true,  // 是否开启CC攻击防护
        'enableSeoIpCheck' => true,  // 开启SEO-IP检查,避免误伤百度蜘蛛等优化IP,此功能需要配合cli.php示例配置
        'seoFriendly' => true,  // 是否SEO友好,不需要SEO的网站请关闭,防御效果更好
        'reqTime' => 60,  // 请求时间间隔,单位秒
        'reqCount' => 30,  // 单位时间内请求次数限制
        'reqDayMaxCount' => 365,  // 每日请求最大次数限制,0则不限制(单个IP限制)
    ],
    'illegalReqCallback' => function ($businessCode) use ($redis) {
        // 拦截后的回调函数,根据业务码进行业务处理,可不配置
        // businessCode说明
        // 100 触发请求频繁  101 触发每日请求最大次数限 102 GET参数包含非法字符串 103 POST参数包含非法字符串 104 COOKIE参数包含非法字符串
        // 105 UserAgent为空  106 UserAgent包含非法字符串  107 禁止代理IP访问  108 请求方法不合法,例如禁止POST请求
        // 例子, 将触发的IP加入IP检查队列,CLI进程需开启,然后自动拉黑
        $checkCode = [100, 101, 105, 106, 107];
        if (in_array($businessCode, $checkCode)) {
            $ip = Utils::getClientRealIp();
            if ($ip) {
                $redis->lPush(Enums::WafCheckIpDnsQueueKey, $ip);
            }
        }
    },
    'checkGet' => true,  // 是否开启GET参数防护,如SQL注入
    'checkPost' => true,  // 是否开启POST参数防护,如SQL注入
    'checkCookie' => true,  // 是否开启COOKIE参数防护,如SQL注入
    'checkUserAgent' => true,  // 是否开启UserAgent参数防护,如各种垃圾爬虫,扫描器等
    'checkProxyRequest' => true,  // 是否开启代理请求检查,拦截代理请求
    'checkRequestMethod' => ['get', 'post', 'option'],  // 是否开启请求方法检测,仅允许get,post,option请求方法
];
(new Waf($wafConfig))->run();
~~~

## <h5>【B】. 独立进程模式检测真假SEO蜘蛛IP </h5>
~~~
// 连接Redis
$redis = new Redis();
$redis->connect('127.0.0.1');

// IP白名单和黑名单有效期
$expire = 86400 * 365;

// 读取队列数据
while (true) {
    // 读取Ip
    $ipData = $redis->brPop(Enums::WafCheckIpDnsQueueKey, 15);
    if (!$ipData) {
        continue;
    }
    $ip = $ipData['1'];
    echo '正在检查IP: ' . $ip . PHP_EOL;
    // 如果IP在白名单,跳过
    $ipWhiteKey = Utils::getIpCacheKey($ip, Enums::WafIpWhiteListKey);
    if ($redis->get($ipWhiteKey)) {
        echo '此IP: ' . $ip . '命中白名单,跳过' . PHP_EOL;
        continue;
    }

    // 如果IP在黑名单,跳过
    $ipBlackKey = Utils::getIpCacheKey($ip, Enums::WafIpBlackListKey);
    if ($redis->get($ipBlackKey)) {
        echo '此IP: ' . $ip . '命中黑名单,跳过' . PHP_EOL;
        continue;
    }

    // 查询IP-DNS信息
    $checkIpCount = 3;
    $dns = '';
    while ($checkIpCount--) {
        $dns = Utils::reverseDnsLookup($ip);
        if ($dns) {
            break;
        }
    }

    // 根据IP查询结果处理
    if (!$dns) {
        echo '此IP: ' . $ip . '查询DNS失败,已添加黑名单' . PHP_EOL;
        $redis->setex($ipBlackKey, $expire, true);
    } else {
        if (!Utils::isSeoFriendlyDns($dns)) {
            // IP黑名单
            echo '此IP: ' . $ip . '伪造SEO蜘蛛,已添加黑名单' . PHP_EOL;
            $redis->setex($ipBlackKey, $expire, true);
        } else {
            // IP白名单
            echo '此IP: ' . $ip . '为真正的SEO蜘蛛,已添加白名单' . PHP_EOL;
            $redis->setex($ipWhiteKey, $expire, true);
        }
    }
}
~~~