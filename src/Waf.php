<?php

namespace EasyWaf;

class Waf
{
    /**
     * constructor.
     */
    public function __construct($config = [])
    {
        Config::set(Enums::WafConfig, $config);
    }

    /**
     * run
     */
    public function run()
    {
        if (!Utils::isCli()) {
            new Check();
        }
    }
}
