<?php

namespace EasyWaf;

class Enums
{
    /**
     * waf settings
     */
    const WafConfig = "wafConfig";

    /**
     * WafCheckIpDnsQueueKey
     */
    const WafCheckIpDnsQueueKey = "wafCheckIpDnsQueueKey";

    /**
     * WafCheckIpBehaviorKey
     */
    const WafCheckIpBehaviorKey = "wafCheckIpBehaviorKey";

    /**
     * WafIpWhiteListKey
     */
    const WafIpWhiteListKey = "wafIpWhiteListKey";

    /**
     * WafIpBlackListKey
     */
    const WafIpBlackListKey = "wafIpBlackListKey";
}