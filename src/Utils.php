<?php

namespace EasyWaf;

class Utils
{
    /**
     * isCli
     * @return bool
     */
    public static function isCli()
    {
        if (php_sapi_name() === 'cli') {
            return true;
        }
        return false;
    }

    /**
     * match
     * @param $data
     * @param $filter
     * @return string
     */
    public static function match($data, $filter)
    {
        $char = '';
        foreach ($data as $key => $value) {
            if (preg_match("/" . $filter . "/is", $key, $res) == 1) {
                $char = $res['0'];
                break;
            }
            $value = self::objectToString($value);
            if (preg_match("/" . $filter . "/is", $value, $res) == 1) {
                $char = $res['0'];
                break;
            }
        }
        return $char;
    }

    /**
     * objectToString
     * @param $object
     * @return mixed|string
     */
    public static function objectToString($object)
    {
        static $result;
        static $keyText;
        if (!is_array($object)) {
            return $object;
        }
        foreach ($object as $key => $val) {
            $keyText = $keyText . $key;
            if (is_array($val)) {
                self::objectToString($val);
            } else {
                $result[] = $val . $keyText;
            }
        }
        return implode($result);
    }


    /**
     * isProxyRequest
     * @return bool
     */
    public static function isProxyRequest()
    {
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return true;
        }
        if (!empty($_SERVER['HTTP_VIA'])) {
            return true;
        }
        if (!empty($_SERVER['HTTP_FORWARDED'])) {
            return true;
        }
        if (!empty($_SERVER['HTTP_X_REAL_IP'])) {
            return true;
        }
        return false;
    }

    /**
     * getRealIp
     * @return mixed
     */
    public static function getClientIp()
    {
        return empty($_SERVER['REMOTE_ADDR']) ? '' : $_SERVER['REMOTE_ADDR'];
    }

    /**
     * getRealIp
     * @return mixed
     */
    public static function getClientRealIp()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            // client,proxy1,proxy2,proxy3
            $ipLink = $_SERVER['HTTP_X_FORWARDED_FOR'];
            $ipLinkNode = explode(',', $ipLink);
            $clientIp = $ipLinkNode['0'] ?? '';
            if ($clientIp) {
                $ip = $clientIp;
            }
        }
        return $ip;
    }

    /**
     * 是否是搜索引擎蜘蛛特征
     * @return bool
     */
    public static function isSearchEnginesSpiderFeature()
    {
        // 是否是蜘蛛
        $isSpider = false;
        $userAgent = strtolower($_SERVER['HTTP_USER_AGENT'] ?? '');

        // 是否是百度蜘蛛
        $allKey = [
            "BaiduSpider",
            "GoogleBot",
            "GoogleOther",
            "360Spider",
            "spider",
            "yiSouSpider",
            "bingBot",
            "youDaoBot",
            "byteSpider",
            "PetalBot",
            "netEstate"
        ];
        foreach ($allKey as $key) {
            $key = strtolower($key);
            if (strpos($userAgent, $key) !== false) {
                $isSpider = true;
                break;
            }
        }
        return $isSpider;
    }

    /**
     * 通过IP反向查询域名
     * @param $ip
     * @return false|string
     */
    public static function reverseDnsLookup($ip)
    {
        $host = gethostbyaddr($ip);
        if ($host != $ip) {
            return $host;
        } else {
            return false;
        }
    }

    /**
     * isSeoFriendlyDns
     * @param $dns
     * @return bool
     */
    public static function isSeoFriendlyDns($dns): bool
    {
        $allKeyWords = [
            'baidu', // 百度蜘蛛
            'hn.kd.ny.adsl', // 360蜘蛛
            'sogouspider', // 搜狗蜘蛛
            'bytespider', // 今日头条蜘蛛
            'msn.com', // 必应蜘蛛
            'google',  // 谷歌蜘蛛
        ];
        foreach ($allKeyWords as $keyWord) {
            if (stripos($dns, $keyWord) !== false) {
                return true;
            }
        }
        return false;
    }

    /**
     * isWin
     * @return bool
     */
    public static function isWin()
    {
        return DIRECTORY_SEPARATOR == '\\';
    }

    /**
     * getIpCacheKey
     * @param $ip
     * @param $type
     * @return string
     */
    public static function getIpCacheKey($ip, $type): string
    {
        return md5($ip . $type);
    }

    /**
     * setCookie
     * @param $name
     * @param $value
     * @param $expire
     * @param $path
     * @param $domain
     * @param $secure
     * @param $httpOnly
     * @return void
     */
    public static function setCookie($name, $value, $expire = 0, $path = '/', $domain = '', $secure = false, $httpOnly = false)
    {
        setcookie($name, $value, $expire, $path, $domain, $secure, $httpOnly);
    }

    /**
     * getCookie
     * @param $name
     * @return mixed|null
     */
    public static function getCookie($name)
    {
        return $_COOKIE[$name] ?? null;
    }
}
