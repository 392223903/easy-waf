<?php

namespace EasyWaf;

class Exception extends \Exception
{
    public function __construct($message, $businessCode = 0, Exception $previous = null)
    {
        parent::__construct($message, $businessCode, $previous);
    }
}